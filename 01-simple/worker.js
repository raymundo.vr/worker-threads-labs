const { parentPort } = require('worker_threads');

console.log("Inside worker");
parentPort.on('message', (message) => {
    console.log("Got this from parent...", message);
    parentPort.postMessage('Bye bye!');
    process.exit(0);
});