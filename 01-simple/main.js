const { Worker } = require('worker_threads');

console.log("Main thread");
const worker = new Worker('./worker.js');
worker.postMessage('Hey worker');
worker.on('message', (message) => {
    console.log("Worker sent this", message);
    worker.postMessage('dong');
});
worker.on('exit', (message) => {
    console.log("Worker is asking to end...", message);
    process.exit(0);
});
