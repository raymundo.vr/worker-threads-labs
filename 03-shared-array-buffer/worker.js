const { parentPort } = require('worker_threads');

parentPort.on('message', (payload) => {
    console.log("I am worker", payload.index, "and will modify my element in shared buffer");
    const sharedView = payload.data;
    //360 degrees bitwise
    sharedView[payload.index] = (payload.index + 1) << 32;

    parentPort.postMessage({ index: payload.index, data: 'done' });

    process.exit(0);
});