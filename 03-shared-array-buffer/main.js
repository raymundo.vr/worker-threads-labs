const { Worker } = require('worker_threads');

console.log("I'm creating a buffer with size 6 *", Int32Array.BYTES_PER_ELEMENT);
const buffer = new SharedArrayBuffer(6 * Int32Array.BYTES_PER_ELEMENT);
const sharedView = new Int32Array(buffer);

for (let i = 0; i < 6; i++) {
    const worker = new Worker('./worker.js');
    worker.postMessage({ index: i, data: sharedView });

    worker.on('message', (message) => {
        console.log("Got from worker", message);
        console.log("Shared buffer looks like", sharedView);
    });
}