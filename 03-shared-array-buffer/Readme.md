# SharedArrayBuffer

Create a [SharedArrayBuffer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SharedArrayBuffer) with a [Int32](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Int32Array) view and allow each worker to modify a corresponding element in the array.

Each worker will modify the element corresponding to its own index. The value is index + 1 leftbitwise 32 (to check the Int32).

The code will print something like

```javascript
I'm creating a buffer with size 6 * 4
I am worker 4 and will modify my element in shared buffer
Got from worker { index: 4, data: 'done' }
Shared buffer looks like Int32Array(6) [ 1, 2, 3, 0, 5, 6 ]
I am worker 5 and will modify my element in shared buffer
Got from worker { index: 5, data: 'done' }
Shared buffer looks like Int32Array(6) [ 1, 2, 3, 0, 5, 6 ]
Got from worker { index: 1, data: 'done' }
Shared buffer looks like Int32Array(6) [ 1, 2, 3, 0, 5, 6 ]
I am worker 1 and will modify my element in shared buffer
Got from worker { index: 0, data: 'done' }
Shared buffer looks like Int32Array(6) [ 1, 2, 3, 4, 5, 6 ]
I am worker 0 and will modify my element in shared buffer
Got from worker { index: 2, data: 'done' }
Shared buffer looks like Int32Array(6) [ 1, 2, 3, 4, 5, 6 ]
I am worker 2 and will modify my element in shared buffer
I am worker 3 and will modify my element in shared buffer
Got from worker { index: 3, data: 'done' }
Shared buffer looks like Int32Array(6) [ 1, 2, 3, 4, 5, 6 ]
```

Notice the asynchronicity with the operation and the message ack.