# Multiple workers on multiple channels

This code prints something like
```
Syncing ports
Worker sent {
  topic: 'message',
  data: 'Hello parent! Greetings from 1',
  timestamp: 1618491597049,
  diffms: 45
}
Got from parent {
  topic: 'message',
  data: 'Hello there worker 1!',
  timestamp: 1618491597004
}
Sending reply
Worker sent {
  topic: 'message',
  data: 'Hello parent! Greetings from 3',
  timestamp: 1618491597051,
  diffms: 45
}
Syncing ports
Syncing ports
Worker sent {
  topic: 'message',
  data: 'Hello parent! Greetings from 5',
  timestamp: 1618491597053,
  diffms: 46
}
Syncing ports
Worker sent {
  topic: 'message',
  data: 'Hello parent! Greetings from 0',
  timestamp: 1618491597051,
  diffms: 47
}
Syncing ports
Syncing ports
Got from parent {
  topic: 'message',
  data: 'Hello there worker 3!',
  timestamp: 1618491597006
}
Sending reply
Got from parent {
  topic: 'message',
  data: 'Hello there worker 5!',
  timestamp: 1618491597007
}
Sending reply
Got from parent {
  topic: 'message',
  data: 'Hello there worker 0!',
  timestamp: 1618491597004
}
Sending reply
Worker sent {
  topic: 'message',
  data: 'Hello parent! Greetings from 4',
  timestamp: 1618491597056,
  diffms: 50
}
Got from parent {
  topic: 'message',
  data: 'Hello there worker 4!',
  timestamp: 1618491597006
}
Sending reply
Worker sent {
  topic: 'message',
  data: 'Hello parent! Greetings from 2',
  timestamp: 1618491597056,
  diffms: 51
}
Got from parent {
  topic: 'message',
  data: 'Hello there worker 2!',
  timestamp: 1618491597005
}
Sending reply
I'm quitting
I'm quitting
I'm quitting
I'm quitting
I'm quitting
I am also leaving...
```