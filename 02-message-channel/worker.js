const { parentPort } = require('worker_threads');

let myPort;
let myId;

setTimeout(() => { console.log("I'm quitting"); process.exit(0); }, 1000);

parentPort.on('message', (payload) => {
    if (!payload.topic) {
        console.warn("Got an invalid thingy", payload);
        return;
    } else if (payload.topic === 'sync') {
        console.log("Syncing ports");
        myPort = payload.port;
        myId = payload.id;
        return;
    }
    console.log("Got from parent", payload);
    const now = (new Date()).getTime();
    const diffms = now - payload.timestamp;
    console.log("Sending reply");
    myPort.postMessage({ topic: 'message', data: `Hello parent! Greetings from ${myId}`, timestamp: now, diffms });
});