const { MessageChannel, Worker } = require('worker_threads');

for (let i = 0; i < 6; i++) {
    // The app that created the channel uses port1 and the app at the other end uses port2.
    const { port1: mainPort, port2: workerPort } = new MessageChannel();
    const worker = new Worker('./worker.js');

    const syncPort = (port) => ({ topic: 'sync', port, id: i });

    mainPort.on('message', (payload) => {
        console.log("Worker sent", payload);
    });

    worker.on('exit', () => {
        console.log("I am also leaving...");
        process.exit(0);
    });

    //Sync port first
    //Generate message
    const syncMessage = syncPort(workerPort);
    //Send message with port and list it in transferList
    worker.postMessage(syncMessage, [workerPort]);
    //Send something next
    worker.postMessage({ topic: 'message', data: `Hello there worker ${i}!`, timestamp: (new Date()).getTime() });
}